# CarStore Project

*This is website project that is created in Magento eCommerce platform and Docker.*

## Requirements

For this project you need to do the following applications installed:

1. Download and install **Docker.** You can download the Docker from the [official website](https://www.docker.com).
2. GitBash from Git installation [https://git-scm.com/downloads]

## How to setup - automatically

1. Download the project folder.
2. In the project folder run the install-script.sh using GitBash.

```bash
./install-script.sh
```
3. **Video demonstration**
   
   [<img src="https://img.youtube.com/vi/RENBDkVdSYE/maxresdefault.jpg" width="43%">](https://www.youtube.com/watch?v=RENBDkVdSYE&feature=youtu.be)
   

## How to setup - manually

If the auto setup script is not working you can setup the application manually using this [instructions](./manual-steps.md).

## Additional resources

Click [here](https://magento.com) for more info about **Magento eCommerce platform.**

Click [here](https://www.docker.com/) for more info about **Docker.**