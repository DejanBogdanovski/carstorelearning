## Manual steps: 
1.Open GitBash inside the downloaded sorcecode folder and start the containers with the command bellow.
```
docker-compose up -d
```
2.Open localhost on your web browser and wait for the installation of Magento, if the installation is completed successfully, Magento luma home page will appear.

3.Place the code folder in your container with this command: 
```
docker cp code magento_conta:/opt/bitnami/magento/htdocs/app
```
4.Enter the container:
```
winpty docker exec -it magento_conta bash
```
5.Go inside the /htdocs/ folder:
```
cd /opt/bitnami/magento/htdocs/
```
6.Unlink /app/etc:
```
unlink ./app/etc
cp -r /bitnami/magento/htdocs/app/etc app/etc
```
7.Enable the module:
```
bin/magento module:enable Dejan_NovModul
bin/magento setup:upgrade
bin/magento setup:di:compile
```
8.After the completion of the steps above, open the link below in your browser to see the result.
```
http://localhost/dejan/index/index
```
###### Click [here](https://magento.com) for more info about **Magento eCommerce platform.**
###### Click [here](https://www.docker.com/) for more info about **Docker.**	